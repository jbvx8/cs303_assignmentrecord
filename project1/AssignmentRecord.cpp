#include "AssignmentRecord.h"
#include "Assignment.h"

Assignment AssignmentRecord::createAssignment(string unparsed)
{
	String_Tokenizer tokenizer(unparsed, ",");
	Date newDueDate = tokenizer.next_token();
	string newDescription = tokenizer.next_token();
	Date newAssignedDate = tokenizer.next_token();
	string nextStatus = tokenizer.next_token();
	Assignment::Status newStatus = getNewStatus(nextStatus);
	return Assignment(newDueDate, newAssignedDate, newDescription, newStatus);
}

/** Function to remove the leading and trailing spaces from a string */
string AssignmentRecord::trim(const string& the_string)
{
	size_t p = the_string.find_first_not_of(" ");
	if (p == string::npos) return "";
	size_t q = the_string.find_last_not_of(" ");
	return the_string.substr(p, q - p + 1);
}

void AssignmentRecord::loadAssignments(ifstream& in)
{
	string line;
	while (getline(in, line))
	{
		String_Tokenizer tokenizer(line, ",");
		Date newDueDate, newAssignedDate;
		// takes all tokens as a string first so they can be saved in case an exception is thrown during conversion to date
		string dueDateToken = trim(tokenizer.next_token());
		string newDescription = trim(tokenizer.next_token());
		string assignedDateToken = trim(tokenizer.next_token());
		string nextStatus = trim(tokenizer.next_token());
		try
		{
			newDueDate = dueDateToken;		// will throw exception if either date is bad
			newAssignedDate = assignedDateToken;
		}
		catch (exception badDate)
		{
			cout << "Error adding assignment with assigned date " << assignedDateToken << ". " << badDate.what() << endl;
			continue;
		}
		try
		{
			Assignment::Status newStatus = getNewStatus(nextStatus); // converts string to enumeration type
			Assignment newAssignment(newDueDate, newAssignedDate, newDescription, newStatus);
			checkValid(newAssignment); // checks whether the dates are in the appropriate order and the status is valid
			addAssignment(newAssignment);
		}
		catch (AssignmentRecordException ae)
		{
			cout << "Error adding assignment with assigned date " << assignedDateToken << ". " << ae.what() << endl;
		}
	}
}

// converts string status to enumeration
Assignment::Status AssignmentRecord::getNewStatus(string strStatus) 
{
	if (strStatus == "assigned")
		return Assignment::Status::assigned;
	else if (strStatus == "completed")
		return Assignment::Status::completed;
	else if (strStatus == "late")
		return Assignment::Status::late;
	else
		throw AssignmentRecordException("Not a valid status");
}

void AssignmentRecord::insertAssignment(const Assignment& assignment, list<Assignment>& assignmentList)
{
	if (assignmentList.size() == 0)
	{
		assignmentList.push_back(assignment);
	}
	else
	{
		list<Assignment>::iterator itr = assignmentList.begin();
		while (itr != assignmentList.end() && *itr < assignment)
		{
			++itr;
		}
		if (itr == assignmentList.end())
		{
			assignmentList.push_back(assignment);
		}
		else if (assignment != *itr) // assignment does not already exist
		{
			assignmentList.insert(itr, assignment);
		}
		else
		{
			cout << "Assignment with assigned date already exists. New assignment not added." << endl;
		}
	}		
}

// iterates for specific assigned date and returns the position
list<Assignment>::iterator AssignmentRecord::findAssignment(const Date& targetDate, list<Assignment>& assignmentList)
{
	for (list<Assignment>::iterator itr = assignmentList.begin(); itr != assignmentList.end(); ++itr)
	{
		if (itr->getAssignedDate() == targetDate)
		{
			return itr;
		}		
	}
	return assignmentList.end();
}


bool AssignmentRecord::checkDateOrder(Date& lhs, Date& rhs)
{
	return (lhs > rhs);
}

void AssignmentRecord::checkValid(Assignment& assignment)
{
	Date assignedDate = assignment.getAssignedDate();
	Date dueDate = assignment.getDueDate();
	if (!assignedDate.valid_date(assignedDate.getYear(), assignedDate.getMonth(), assignedDate.getDay())) // doesn't get here
	{
		throw AssignmentRecordException("Assigned date is invalid.");
	}
	else if (!dueDate.valid_date(dueDate.getYear(), dueDate.getMonth(), dueDate.getDay()))
	{
		throw AssignmentRecordException("Due date is invalid.");
	}
	else if (!checkDateOrder(dueDate, assignedDate))
	{
		throw AssignmentRecordException("Due date cannot be before or equal to the assigned Date.");
	}
	else if (find(assignedList.begin(), assignedList.end(), assignment) != assignedList.end() || find(completedList.begin(), completedList.end(), assignment) != completedList.end())
	{
		throw AssignmentRecordException("Assignment with given assigned date already exists in record.");
	}
}

// everything should be valid before inserting assignment
void AssignmentRecord::addAssignment(Assignment& assignment)
{
	if (assignment.getStatus() == Assignment::Status::assigned)
	{
		insertAssignment(assignment, assignedList);
	}
	else if (assignment.getStatus() == Assignment::Status::completed)
	{
		insertAssignment(assignment, completedList);
	}
	else if (assignment.getStatus() == Assignment::Status::late)
	{
		insertAssignment(assignment, completedList);
		lateCount++;
	}
}

// Iterates through list and returns assignment with given assigned date
Assignment AssignmentRecord::lookupAssignment(const Date& assignedDate, list<Assignment>& assignmentList)
{
	list<Assignment>::iterator itr = findAssignment(assignedDate, assignmentList);
	if (itr == assignmentList.end())
	{
		throw AssignmentRecordException("No assignment found for the given date.");
	}
	return *itr;
}

// prints assignments to screen in same format as input file
void AssignmentRecord::displayAssignments()
{
	if (assignedList.size() > 0)
	{
		cout << "Assigned Assignments: " << endl;
		for (list<Assignment>::iterator itr = assignedList.begin(); itr != assignedList.end(); ++itr)
		{
			cout << *itr << endl;
		}
	}
	if (completedList.size() > 0)
	{
		cout << endl << "Completed Assignments: " << endl;
		for (list<Assignment>::iterator iter = completedList.begin(); iter != completedList.end(); ++iter)
		{
			cout << *iter << endl;
		}
	}	
}

void AssignmentRecord::completeAssignment(Date& assignedDate, Date& completedDate)
{
	Assignment temp = lookupAssignment(assignedDate, assignedList);
	if (temp.getDueDate() < completedDate)
	{
		temp.setStatus(Assignment::Status::late);
		lateCount++;
	}
	else
	{
		temp.setStatus(Assignment::Status::completed);
	}
	insertAssignment(temp, completedList); // adds completed assignment to completed list
	assignedList.remove(temp); // removes from assigned list
}

void AssignmentRecord::changeDueDate(const Date& assignmentDate, const Date& newDueDate)
{
	list<Assignment>::iterator itr = findAssignment(assignmentDate, assignedList);
	if (itr != assignedList.end())
	{
		itr->setDueDate(newDueDate); 
	}
	else
	{
		cout << "No assignment found with that date." << endl;
	}
	
}

void AssignmentRecord::changeDescription(const Date& assignmentDate, const string& newDescription)
{
	list<Assignment>::iterator itr = findAssignment(assignmentDate, assignedList);
	if (itr != assignedList.end())
	{
		itr->setDescription(newDescription);
	}
	else
	{
		cout << "No assignment found with that date." << endl;
	}
	
}