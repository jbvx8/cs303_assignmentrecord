#include "UserInterface.h"
#include <string>
using namespace std;

void UserInterface::processCommands()
{
	size_t choice;
	do
	{
		cout << "\nMENU" << endl
			 << "1: Load assignments from file" << endl
			 << "2: Display assignments" << endl
			 << "3: Add assignment" << endl
			 << "4: Edit assignment" << endl
			 << "5: Complete assignment" << endl
			 << "6: Display number of late assignments" << endl
			 << "7: Save assignments to assignment file" << endl
			 << "8: Exit" << endl << endl;
		cout << "Enter your choice (1-8): ";
		cin >> choice;
		cout << endl;
		
		switch (choice)
		{
		case 1: loadAssignments(); break;
		case 2: record.displayAssignments(); break;
		case 3: addAssignment(); break;
		case 4: editAssignment(); break;
		case 5: completeAssignment(); break;
		case 6: showLateCount(); break;
		case 7: save(); break;
		}
	} while (choice != 8);
} 

void UserInterface::loadAssignments()
{
	ifstream fin = openFile();
	record.loadAssignments(fin);
	fin.close();
}

ifstream UserInterface::openFile()
{
	string filename;
	ifstream fin;
	// loops while file not found
	do
	{
		cout << "Enter file name: ";
		cin >> filename;
		fin.open(filename);
		if (!fin)
		{
			cout << "File could not be opened." << endl;
		}
	} while (!fin);
	return fin;
}

void UserInterface::addAssignment()
{
	Date userAssigned, userDue;
	string userDescription, strStatus;
	Assignment::Status userStatus;
	bool valid = false;
	
	while (valid == false)
	{
		do
		{
			cout << "Enter an assigned date: ";
			try
			{
				cin >> userAssigned;
				valid = true;
			}
			catch (exception badDate)
			{
				cout << "Error: " << badDate.what() << endl << endl;
			}
		} while (valid == false);
		valid = false;
		do
		{
			cout << "Enter a due date: ";
			try
			{
				cin >> userDue;
				valid = true;
			}
			catch (exception badDate)
			{
				cout << "Error: " << badDate.what() << endl << endl;
			}
		} while (valid == false);
		if (userDue <= userAssigned) // makes sure the due date is later than assigned date
		{
			valid = false;
			cout << "Due date must be later than assigned date." << endl << endl;
		}
	}
	cout << "Enter a description: ";
	cin >> userDescription;
	do  // repeats until a valid status is entered
	{
		try
		{
			cout << "Enter a status (assigned, completed, or late): ";
			cin >> strStatus;
			userStatus = record.getNewStatus(strStatus); // converts a string status to enumeration type
		}
		catch (AssignmentRecord::AssignmentRecordException ae)
		{
			cout << "Error: " << ae.what() << endl << endl;
		}
		if (userStatus == Assignment::Status::late)
		{
			record.incrementLateCount();
		}
	} while (userStatus == Assignment::Status::unassigned);
	
	Assignment userAssignment(userDue, userAssigned, userDescription, userStatus); // constructs a new assignment with all the entered data, which should now be valid
	record.addAssignment(userAssignment); //adds the assignment into the appropriate list
}

void UserInterface::showLateCount()
{
	cout << endl << "Number of late assignments: " << record.getLateCount() << endl << endl; // prints the late count to the screen
}

void UserInterface::completeAssignment()
{
	Date assignmentDate, completionDate;
	bool notValid = true;
	do // loops until the user enters a valid date
	{
		cout << "Enter the assignment date for the assignment you wish to complete: ";
		try
		{
			cin >> assignmentDate;
			notValid = false;
		}
		catch (exception badDate)
		{
			cout << badDate.what() << endl;
		}
		
	} while (notValid == true);
	notValid = true;
	do  // loops until the user enters a valid date
	{
		cout << "Enter the completion date: ";
		try
		{
			cin >> completionDate;
			notValid = false;
		}
		catch (exception badDate)
		{
			cout << badDate.what() << endl;
		}
	} while (notValid == true);
	try
	{
		record.completeAssignment(assignmentDate, completionDate); // will throw exception if assignment not found
	}
	catch (AssignmentRecord::AssignmentRecordException ae)
	{
		cout << ae.what() << endl;
	}
}

void UserInterface::editAssignment()
{
	bool notValid = true;
	Date assignmentDate, newDueDate;
	string newDescription;
	size_t choice = 0;
	
	while (choice != 1 && choice != 2) // loops until a valid choice is made
	{
		cout << "Which would you like to edit? Choose 1 or 2: \n" <<
			"1. Due date\n" <<
			"2. Description\n";
		cin >> choice;
		if (choice != 1 && choice != 2)
		{
			cout << "Entry not valid. Try again." << endl;
		}
	}
	do // loops until a valid date is entered
	{
		cout << "Edit the assigned date for the assignment you want to edit: ";
		try
		{
			cin >> assignmentDate;
			notValid = false;
		}
		catch (exception badDate)
		{
			cout << badDate.what() << endl;
		}
	} while (notValid == true);
	
	if (choice == 1) // user wants to edit due date
	{
		notValid = true;
		do // loops until a valid date is entered
		{
			cout << "Enter a new due date: ";
			try
			{
				cin >> newDueDate;
				notValid = false;
			}
			catch (exception badDate)
			{
				cout << badDate.what() << endl;
			}
		} while (notValid == true);
		try
		{
			record.changeDueDate(assignmentDate, newDueDate); // will throw exception if assignment not found
		}
		catch (AssignmentRecord::AssignmentRecordException notFound)
		{
			cout << "Error: " << notFound.what() << endl;
		}
	}
	else // user wants to edit description
	{
		cout << "Enter a new description: ";
		cin >> newDescription;
		try
		{
			record.changeDescription(assignmentDate, newDescription); // will throw exception if assignment not found
		}
		catch (AssignmentRecord::AssignmentRecordException notFound)
		{
			cout << "Error: " << notFound.what() << endl;
		}
	}
}

void UserInterface::save()
{
	ofstream fout;
	string outfile;
	cout << "Enter name of file to save: ";
	cin >> outfile;
	fout.open(outfile);
	list<Assignment> assignedList = record.getAssignedList();
	list<Assignment> completedList = record.getCompletedList();
	for (list<Assignment>::iterator itr = assignedList.begin(); itr != assignedList.end(); ++itr) // prints out all entries in the list in same format as the input file
		fout << *itr << endl;
	for (list<Assignment>::iterator iter = completedList.begin(); iter != completedList.end(); ++iter)
		fout << *iter << endl;
	fout.close();
}