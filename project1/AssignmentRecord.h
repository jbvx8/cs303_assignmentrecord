#ifndef ASSIGNMENT_RECORD_H
#define ASSIGNMENT_RECORD_H

#include "Assignment.h"
#include<list>
#include<iostream>
#include<fstream>
#include<iterator>
using namespace std;

class AssignmentRecord
{
public:
	string trim(const string& the_string);
	void loadAssignments(ifstream& in);
	Assignment::Status getNewStatus(string strStatus);
	Assignment createAssignment(string unparsed);
	void insertAssignment(const Assignment& assignment, list<Assignment>& assignmentList);
	bool checkDateOrder(Date& lhs, Date& rhs);
	void checkValid(Assignment& assignment);
	void addAssignment(Assignment& assignment);
	list<Assignment>::iterator findAssignment(const Date& targetDate, list<Assignment>& assignmentList);
	Assignment lookupAssignment(const Date& assignedDate, list<Assignment>& assignmentList);
	void displayAssignments();
	void completeAssignment(Date& assignedDate, Date& completedDate);
	void changeDueDate(const Date& assignmentDate, const Date& newDueDate);
	void changeDescription(const Date& assignmentDate, const string& newDescription);
	size_t getLateCount() const { return lateCount; }
	void setLateCount(size_t newLateCount) { lateCount = newLateCount; }
	void incrementLateCount() { lateCount++; }
	list<Assignment> getAssignedList() const { return assignedList; }
	list<Assignment> getCompletedList() const { return completedList; }

	class AssignmentRecordException
	{
	public:
		AssignmentRecordException(string m = "") { message = m; }
		string what() const { return message; }
	private:
		string message;
	};

	
private:
	list<Assignment> assignedList;
	list<Assignment> completedList;
	size_t lateCount;
};

#endif
