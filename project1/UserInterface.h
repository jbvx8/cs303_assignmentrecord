#ifndef U_INTERFACE_H
#define U_INTERFACE_H

#include "AssignmentRecord.h"

class UserInterface
{
public:
	UserInterface(AssignmentRecord& aRecord) : record(aRecord) {}
	UserInterface() {}
	void processCommands();
	//PRE: none
	//POST: the user has completed all assignment commands and exited the program
	//Input: numerical commands from the user
	//This method provides the menu for the user to enter their commands, and appropriate methods are called accordingly

	void loadAssignments();
	//PRE: none
	//POST: assignments have been loaded from a user-entered file and have populated the assigned and completed lists.
	//Error handling: checks for a valid input file, but does not check whether data in the file are in the correct format.

	ifstream openFile();
	//PRE: none
	//POST: a file stream is open and ready to be read
	//Input: string filename
	//Error handling: if file cannot be opened, user is prompted for a new name until a file can be opened.

	void addAssignment();
	//PRE: none
	//POST: a new assignment is added to either the assigned or completed list
	//Input: assignment date, due date, description (string), status (string converts to enum)
	//Error handling: catches exceptions for bad dates or statuses, also whether the due date is earlier than assignment date
	//Assignment not added in these cases.

	void showLateCount();
	//PRE: a late count that keeps track of the number of late assignments
	//POST: the value is printed to the screen

	void completeAssignment();
	//PRE: assignment should be in assigned list
	//POST: assignment is removed from assigned list and added to completed list as either completed or late
	//Input: assignment date, completed date
	//Error handling: throws exceptions for bad dates, does not complete an assignment that cannot be found.

	void editAssignment();
	//PRE: assignment should be in assigned list
	//POST: due date or description are edited
	//Input: choice of what to edit, assignment date to edit, new due date, new description
	//Error handling: throws exceptions for bad dates, does not edit if assignment not found.

	void save();
	//PRE: lists are populated with data to save
	//POST: lists are output to file in same format as input file
	//Input: output filename

private:
	AssignmentRecord record;
};
#endif
