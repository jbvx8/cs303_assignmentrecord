#ifndef ASSIGNMENT_H
#define ASSIGNMENT_H

#include "Date.h"
#include "StringTokenizer.h"
#include <string>
using namespace std;

class Assignment
{
public:
	
	enum class Status { unassigned, assigned, completed, late };
	

	Assignment(const Date& due, const Date& assigned, const string& assignmentDescription, const Assignment::Status& assignmentStatus) :
		dueDate(due), assignedDate(assigned), description(assignmentDescription), status(assignmentStatus) {} 
	Assignment() : dueDate(1,1,1), assignedDate(1,1,1), description(""), status(Status::unassigned) {}
	Date getDueDate() const { return dueDate; }
	Date getAssignedDate() const { return assignedDate; }
	string getDescription() const { return description; }
	Status getStatus() const { return status; }

	void setDueDate(const Date& newDueDate) { dueDate = newDueDate; }
	void setAssignedDate(const Date& newAssignedDate) { assignedDate = newAssignedDate; }
	void setDescription(const string& newDescription) { description = newDescription; }
	void setStatus(const Status& newStatus) { status = newStatus; }

	string statusToString()
	{
		if (status == Status::assigned)
			return "assigned";
		else if (status == Status::completed)
			return "completed";
		else if (status == Status::late)
			return "late";
		else
			throw exception("Not a valid status.");
	}

	
	friend bool operator<(const Assignment& lhs, const Assignment& rhs) // sorts on due date
	{
		return lhs.dueDate < rhs.dueDate;
	}

	friend bool operator==(const Assignment& lhs, const Assignment& rhs) // finds matching records by assignment date
	{
		return lhs.assignedDate == rhs.assignedDate;
	}

	friend bool operator!=(const Assignment& lhs, const Assignment& rhs)
	{
		return lhs.assignedDate != rhs.assignedDate;
	}

	friend ostream& operator << (ostream& out, Assignment& a)
	{
		out << a.dueDate.toString() << ", " << a.description << ", " << a.assignedDate.toString() << ", " << a.statusToString();
		return out;
	}

private:
	Date dueDate;
	Date assignedDate;
	string description;
	Status status;


};

#endif
